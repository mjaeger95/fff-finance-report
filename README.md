# FFF-Finanzberichtsdatenaufbereitungstool

## How To Use

**Make sure to install python 3.9 or higher before proceeding.**

1. Export data from google sheets live table as CSV (you should have access)
2. Save CSV file as `input.csv` and place it in the same folder as the script
3. Run the script (on Windows: double-click the `main.py` if you have Python installed, the .EXE-file if not)
4. The output will be saved as `output.csv`
5. Import output into GNU Cash

## Remarks

* This script will drop all data sets where column 6 does not start with the exact string "Überwiesen:" and does not process the date in this line at all
* This script will break if this the layout of this table is changed. Contact the author before making changes.
* It was written with Python 3.9.1
* I have compiled it to x64 EXE for convenience. Feel free to produce linux executables as you like.

## License & Contribute

If you want to contribute, please contact the repo owner.

Published under CC BY-SA 4.0:
https://creativecommons.org/licenses/by-sa/4.0/
