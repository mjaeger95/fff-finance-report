import csv
import os
from datetime import datetime
from operator import itemgetter

header = ['Datum', 'OG/AG', 'Kategorie', 'Zweck', 'Betrag', 'Status FFF', 'Status Plant for the Planet']
skipped_lines = 0


def reader(path='input.csv'):
    """
    read data from CSV file
    :param path: path of file to read (qualified or local)
    :return: line generator
    """
    print("Dieses tool extrahiert Daten für den GNU Cash Import aus exportierten Rohdaten")
    print("Name der zu verarbeitenden Datei im selben Ordner (leer lassen für 'input.csv'):")
    entry = input("")
    if entry:
        path = entry
    with open(path, newline='', encoding='utf-8') as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=",")
        first_line = next(csv_reader)
        if not all([found == expected for found, expected in zip(first_line, header)]):
            print("Warnung:  Die Kopfzeile sieht nicht so aus wie sie sollte. Unerwartete Ergebnisse möglich.")
            print("Erwartet: "+str(header)+"\nGefunden: "+str(first_line)+'\n')
        for csv_line in csv_reader:
            yield csv_line


def write(stream, path="output.csv"):
    """
    Write processed data to file. Will overwrite specified file's content.
    :param stream: data to write
    :param path: path of file to write to.
    :return: None
    """
    with open(path, 'w+', newline='', encoding='utf-8') as csvfile:
        print("Ausgabedatei: " + os.getcwd() + '\\' + path)
        writer = csv.writer(csvfile, delimiter=';')
        for line in stream:
            if line[0] != '':  # filter skipped lines
                writer.writerow(line[:4])
        # I like Javascript:
        # stream.filter(line -> line[0] !== '').foreach(writer.writerow)


def process(csv_line):
    """
    process individual data set
    :param csv_line: line as read from CSV file (list)
    :return: line to be written to new CSV file (list)
    """
    global skipped_lines
    recipient = csv_line[1]
    subject = csv_line[3]
    try:
        amount = float(csv_line[4].replace(",", "."))
    except ValueError:
        amount = csv_line[4]
    status = csv_line[6]
    # skip lines that aren't relevant
    if status.startswith("Überwiesen:"):
        try:
            date = datetime.strptime(status[-10:], "%d.%m.%Y")
        except ValueError:
            try:
                date = datetime.strptime(status[-8:], "%d.%m.%y")
                print("Warnung: Datumsformat DD.MM.YY statt DD.MM.YYYY gefunden")
            except ValueError:
                print("Fehler! Datum konnte nicht gelesen werden: ", status)
                date = ""
        status = date.strftime("%d.%m.%Y")
        date = date.strftime("%Y%m%d")
    else:
        skipped_lines += 1
        status = ''
        date = ''
    return [status, recipient, subject, amount, date]


def wrapper():
    data = reader()
    data = map(process, data)
    data = sorted(data, key=itemgetter(4))
    write(data)
    print()
    print(skipped_lines, " Zeilen übersprungen")
    input("(drücke Enter zum beenden)")


if __name__ == '__main__':
    wrapper()
